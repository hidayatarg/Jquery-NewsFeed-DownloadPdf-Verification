//select the odd element
const $odd = $('a:odd');
// odd.hide();

const $secureLinks= $('a[href^="https://"]');

//for testing
//$secureLinks.hide();

const $pdfCheckbox = $('<label><input type="checkbox"> Allow PDF downloads</label>');

const $pdfs= $('a[href$=".pdf"]');

//$pdfs.hide();

//making the secure links target blank so open in a new tab
$secureLinks.attr('target','_blank');

//pdf inplace of opening download in a browser
$pdfs.attr('download',true);


$secureLinks.addClass('secure');
$pdfs.addClass('pdf');

$pdfs.on('click',function(){
    if ($(':checked').length===0){
        event.preventDefault();

        alert('Please check the box to allow PDF download');
    }
    


});

//append the label to the lin container
$('#links').append($pdfCheckbox);


$('a').each(function(){
    const url=$(this).attr('href');
    $(this).parent().append(`(${url})`);
})